# set virtualbox as VAGRANT_DEFAULT_PROVIDER
ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'

MAKE_CMD = "make --directory=/vagrant --makefile=/vagrant/Makefile"

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.
  config.vm.define "web" do |web|
    # Every Vagrant development environment requires a box. You can search for
    # boxes at https://vagrantcloud.com/search.
    web.vm.box = "ubuntu/bionic64"

    # Disable automatic box update checking. If you disable this, then
    # boxes will only be checked for updates when the user runs
    # `vagrant box outdated`. This is not recommended.
    web.vm.box_check_update = false
    web.vm.hostname = "web"


    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine. In the example below,
    # accessing "localhost:8080" will access port 80 on the guest machine.
    # NOTE: This will enable public access to the opened port
    web.vm.network "forwarded_port", guest: 80, host: 8080
    web.vm.network "forwarded_port", guest: 443, host: 8443
    web.vm.network "forwarded_port", guest: 8080, host: 8080

    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine and only allow access
    # via 127.0.0.1 to disable public access
    # web.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

    # Create a private network, which allows host-only access to the machine
    # using a specific IP.
    # web.vm.network "private_network", type: "dhcp"
    web.vm.network "private_network", ip: "172.28.128.11"


    # Create a public network, which generally matched to bridged network.
    # Bridged networks make the machine appear as another physical device on
    # your network.
    # web.vm.network "public_network"

    # Share an additional folder to the guest VM. The first argument is
    # the path on the host to the actual folder. The second argument is
    # the path on the guest to mount the folder. And the optional third
    # argument is a set of non-required options.
    # web.vm.synced_folder ".", "/vagrant_data"
    # web.vm.synced_folder ".", "/vagrant", disabled: false, owner: "nobody", group: "nogroup", create:true
    web.vm.synced_folder ".",
                         "/vagrant",
                         type: "rsync",
                         rsync__exclude: ["venv", ".vagrant", "Vagrantfile", "*.log"],
                         rsync__chown: false,
                         rsync__auto: true,
                         rsync__args: ["--verbose", "--archive"]


    # Provider-specific configuration so you can fine-tune various
    # backing providers for Vagrant. These expose provider-specific options.
    # Example for VirtualBox:

    web.vm.provider "virtualbox" do |vb|
      # Display the VirtualBox GUI when booting the machine
      vb.gui = false
      # Customize the amount of memory on the VM:
      vb.memory = "2048"
      vb.cpus = 2

      # In the example above, the VM is modified to have a host CPU execution cap of 50%, meaning that no matter
      # how much CPU is used in the VM, no more than 50% would be used on your own host machine.
      vb.customize ["modifyvm", :id, "--cpuexecutioncap", "50"]
    end

    # View the documentation for the provider you are using for more
    # information on available options.

    # Enable provisioning with a shell script. Additional provisioners such as
    # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
    # documentation for more information about their specific syntax and use.
    # web.vm.provision "shell", inline: <<-SHELL
    #   apt-get update
    #   apt-get install -y apache2
    # SHELL
    web.vm.provision "shell", inline: "apt-get update && apt-get install -y make"
    #web.vm.provision "shell", inline: "#{MAKE_CMD} config.display", run: "always"

  end

  config.vm.define "db" do |db|
    # Every Vagrant development environment requires a box. You can search for
    # boxes at https://vagrantcloud.com/search.
    db.vm.box = "ubuntu/bionic64"

    # Disable automatic box update checking. If you disable this, then
    # boxes will only be checked for updates when the user runs
    # `vagrant box outdated`. This is not recommended.
    db.vm.box_check_update = false
    db.vm.hostname = "db"

    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine. In the example below,
    # accessing "localhost:8080" will access port 80 on the guest machine.
    # NOTE: This will enable public access to the opened port
    db.vm.network "forwarded_port", guest: 5432, host: 15432
    db.vm.network "forwarded_port", guest: 3306, host: 13306

    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine and only allow access
    # via 127.0.0.1 to disable public access
    # db.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

    # Create a private network, which allows host-only access to the machine
    # using a specific IP.
    # db.vm.network "private_network", type: "dhcp"
    db.vm.network "private_network", ip: "172.28.128.10"


    # Create a public network, which generally matched to bridged network.
    # Bridged networks make the machine appear as another physical device on
    # your network.
    # db.vm.network "public_network"

    # Share an additional folder to the guest VM. The first argument is
    # the path on the host to the actual folder. The second argument is
    # the path on the guest to mount the folder. And the optional third
    # argument is a set of non-required options.
    # db.vm.synced_folder "../data", "/vagrant_data"

    # Provider-specific configuration so you can fine-tune various
    # backing providers for Vagrant. These expose provider-specific options.
    # Example for VirtualBox:

    db.vm.provider "virtualbox" do |vb|
      # Display the VirtualBox GUI when booting the machine
      vb.gui = false
      # Customize the amount of memory on the VM:
      vb.memory = "1024"
      vb.cpus = 1

      # In the example above, the VM is modified to have a host CPU execution cap of 50%, meaning that no matter
      # how much CPU is used in the VM, no more than 50% would be used on your own host machine.
      vb.customize ["modifyvm", :id, "--cpuexecutioncap", "50"]
    end

    # View the documentation for the provider you are using for more
    # information on available options.

    # Enable provisioning with a shell script. Additional provisioners such as
    # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
    # documentation for more information about their specific syntax and use.
    # db.vm.provision "shell", inline: <<-SHELL
    #   apt-get update
    #   apt-get install -y apache2
    # SHELL
    db.vm.provision "shell", inline: "apt-get update && apt-get install -y make"
    db.vm.provision "shell", inline: "#{MAKE_CMD} app.postgres.setup"
    db.vm.provision "shell", inline: "#{MAKE_CMD} app.postgres.config", run: "always"

  end
end
