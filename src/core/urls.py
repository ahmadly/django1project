"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

import rest_framework.authtoken.views
from django.views.generic import RedirectView
from rest_framework_swagger.views import get_swagger_view

# todo use django rest router to create urls map
# from rest_framework import routers
# router = routers.DefaultRouter()

handler400 = 'django.views.defaults.bad_request'
handler403 = 'django.views.defaults.permission_denied'
handler404 = 'django.views.defaults.page_not_found'
handler500 = 'django.views.defaults.server_error'

admin.site.site_title = 'Dashboard'
admin.site.site_header = 'Portal Core'
admin.site.index_title = 'administration'
admin.site.site_url = '/'

urlpatterns = [
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', admin.site.urls),

    # django rest space
    url(r'^api/', include([
        url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
        url(r'^obtain_auth_token/', rest_framework.authtoken.views.obtain_auth_token),
        url(r'^docs/', get_swagger_view(title='Pishro Cloud API'), name='swagger_docs'),
        url(r'^', include([
            url(r'^', include('wrappers.instances.api.v1.urls')),
            url(r'^', include('wrappers.volumes.api.v1.urls')),
            url(r'^', include('wrappers.images.api.v1.urls')),
            url(r'^', include('wrappers.securitygroups.api.v1.urls')),

            url(r'^', include('billing.api.v1.urls')),
            url(r'^', include('accounts.api.v1.urls')),
            url(r'^', include('wallet.api.v1.urls')),
        ]))
    ])),

    # redirect index page to docs page
    url(r'^$', RedirectView.as_view(pattern_name='swagger_docs', permanent=False), name='index')

]
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))
