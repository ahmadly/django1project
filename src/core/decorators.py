import time
from functools import wraps


def print_header(decorated_function):
    """
    print header and footer for function result
    :param decorated_function:
    :return:
    """
    @wraps(decorated_function)
    def wrapper(*args, **kwargs):
        print('=====================[   START   ]=====================', flush=True)
        result = decorated_function(*args, **kwargs)
        print('=====================[    END    ]=====================', flush=True)
        return result

    return wrapper


def timer(decorated_function):
    """
    calculate function run time
    https://gitlab.com/snippets/1734519
    :param decorated_function:
    :return: total run time
    :rtype:object
    """

    @wraps(decorated_function)
    def wrapper(*args, **kwargs):
        _start = time.time()
        result = decorated_function(*args, **kwargs)
        _end = time.time()
        print(f'function: {decorated_function.__name__} > {_end - _start}\'s')
        return result

    return wrapper
