"""
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

"""
from .base import *  # noqa : F403,F401

DEBUG = False
ALLOWED_HOSTS = os.getenv('ALLOWED_HOSTS', '127.0.0.1 145.239.165.157').split(sep=' ')  # noqa : F405
INTERNAL_IPS = os.getenv('INTERNAL_IPS', '127.0.0.1 145.239.165.157').split(sep=' ')  # noqa : F405

SECRET_KEY = os.getenv('SECRET_KEY')
CSRF_COOKIE_SECURE = True
SECURE_HSTS_SECONDS = 1
SESSION_COOKIE_SECURE = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True

SECURE_SSL_REDIRECT = True
X_FRAME_OPTIONS = 'DENY'
SECURE_HSTS_PRELOAD = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',  # noqa : F405
        'NAME': os.getenv('DB_NAME', 'postgresql'),  # noqa : F405
        'USER': os.getenv('DB_USER', 'postgresql'),  # noqa : F405
        'PASSWORD': os.getenv('DB_PASS'),  # noqa : F405
        'HOST': os.getenv('DB_HOST', 'localhost'),  # noqa : F405
        'PORT': os.getenv('DB_PORT', '15432'),  # noqa : F405
        'ATOMIC_REQUESTS': True,
        'CONN_MAX_AGE': 120,
        'AUTOCOMMIT': True,
        'OPTIONS': {},
        'TIME_ZONE': None,
        'DISABLE_SERVER_SIDE_CURSORS': False,
        'TEST': {
            'CHARSET': None,
            'COLLATION': None,
            'NAME': None,
            'MIRROR': None,
        }
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
SMS_BACKEND = 'notification.backends.sms.egs.SMSBackend'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': os.getenv('CACHE_SERVERS', '127.0.0.1:11211').split(sep=' '),  # noqa : F405

        'TIMEOUT': 60,
        'OPTIONS': {
            'MAX_ENTRIES': 10000
        },
    }
}

# Custom logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',  # To capture more than ERROR, change to WARNING, INFO, etc.
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'tags': {'custom-tag': 'x'},
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}

try:
    from .local import *  # noqa : F401
except ImportError:
    pass
