"""
"""
import raven

from .base import *  # noqa : F403,F401

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = ['127.0.0.1', '*']
INTERNAL_IPS = ['127.0.0.1', '*']

#  add debug_toolbar
DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]

DEBUG_TOOLBAR_CONFIG = {}
INSTALLED_APPS.append('debug_toolbar')  # noqa : F405
MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')  # noqa : F405

# corsheaders
INSTALLED_APPS.append('corsheaders')  # noqa : F405
MIDDLEWARE.append('corsheaders.middleware.CorsMiddleware')  # noqa : F405
CORS_ORIGIN_ALLOW_ALL = True

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',  # noqa : F405
        'NAME': os.getenv('DB_NAME', 'postgresql'),  # noqa : F405
        'USER': os.getenv('DB_USER', 'postgresql'),  # noqa : F405
        'PASSWORD': os.getenv('DB_PASS', 'postgresql'),  # noqa : F405
        'HOST': os.getenv('DB_HOST', 'localhost'),  # noqa : F405
        'PORT': os.getenv('DB_PORT', '15432'),  # noqa : F405
    }
}

# Instead of sending out real emails the console backend just writes the emails that would be sent to the
# standard output. By default, the console backend writes to stdout. You can use a different stream-like object
# by providing the stream keyword argument when constructing the connection.
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
SMS_BACKEND = 'notification.backends.sms.console.SMSBackend'

SECRET_KEY = '?e^4GGtezKrv8EPefdP?h=hAmbRjeW'

SENTRY_CONFIG = {
    # 'dsn': 'http://ce52ffe0f0b64709a591142362880408:5b77f16bab2e427e9a813cd4d05ae987''@213.233.176.33:9090//2',
    'release': raven.fetch_git_sha(os.path.dirname(BASE_DIR)),
    'CELERY_LOGLEVEL': os.getenv('CELERY_LOGLEVEL', logging.INFO)}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'TIMEOUT': 1,
        'OPTIONS': {
            'MAX_ENTRIES': 1000
        }
    }
}

try:
    from .local import *  # noqa : F401
except ImportError:
    pass
