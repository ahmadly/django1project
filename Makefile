# Unlike most variables, the variable SHELL is never set from the environment.
# This is because the SHELL environment variable is used to specify your personal
# choice of shell program for interactive use. It would be very bad for personal
# choices like this to affect the functioning of makefiles. See Variables from the Environment.
SHELL=/bin/bash

# project config
PROJECT_NAME:=django1project
PROJECT_DOMAIN:=tdjango1project.com
PROJECT_GIT_BRANCH=$(shell git branch | sed -n -e 's/^\* \(.*\)/\1/p')
PROJECT_ENVIRONMENT:=development
PROJECT_TIMEZONE:=UTC

# project dynamic config
PROJECT_ROOT_DIRECTORY=${CURDIR}
PROJECT_SOURCE_DIRECTORY=${CURDIR}/src
PROJECT_VENV_DIRECTORY=${CURDIR}/venv
PROJECT_DEPLOYMENT_DIRECTORY=${CURDIR}/deployment
PROJECT_LOG_DIRECTORY=${CURDIR}/log
PROJECT_TEMPORARY_DIRECTORY=/tmp
PROJECT_STATIC_FILE=${PROJECT_SOURCE_DIRECTORY}/files
PROJECT_USER:=$(PROJECT_NAME)

# python config
PYTHONCOERCECLOCALE:=UTF-8
PYTHONUTF8:=1
PYTHONDEBUG:=1
PYTHONVERBOSE:=
PYTHONWARNINGS=always
VIRTUAL_ENV=${CURDIR}/venv
PYTHON3:=$(PROJECT_VENV_DIRECTORY)/bin/python3 -u -B -W once

# djanog
DJANGO_SETTINGS_MODULE:=core.settings.$(PROJECT_ENVIRONMENT)
DJANGO_MANAGE_FILE=$(PROJECT_SOURCE_DIRECTORY)/manage.py

# db (if you want to use vagrant db)
DB_HOST=172.28.128.10
DB_PORT=3306
DB_NAME=$(PROJECT_NAME)_$(PROJECT_ENVIRONMENT)_db
DB_USER=$(PROJECT_NAME)_$(PROJECT_ENVIRONMENT)_user
DB_PASS=passwd


# linux
PKG:=apt -y
PATH:=$(VIRTUAL_ENV)/bin:$(PATH)


# Simply by being mentioned as a target,
# this tells make to export all variables to child processes by default.
.EXPORT_ALL_VARIABLES:

# Sets the default goal to be used if no targets were specified on
# the command line (see Arguments to Specify the Goals). The .DEFAULT_GOAL variable allows you
# to discover the current default goal, restart the default goal selection algorithm
# by clearing its value, or to explicitly set the default goal.
.DEFAULT_GOAL := show-help


# if current dir dosn have .git data , make PROJECT_GIT_BRANCH empty
# if PROJECT_GIT_BRANCH was empty make fill it with 'master'
ifeq ($(PROJECT_GIT_BRANCH), )
PROJECT_GIT_BRANCH=master
endif

# if PROJECT_GIT_BRANCH was empty, make fill it with PROJECT_GIT_BRANCH value
ifeq ($(PROJECT_ENVIRONMENT), )
PROJECT_ENVIRONMENT=$(PROJECT_GIT_BRANCH)
endif

# in develop mode (based git branch)
ifeq ($(PROJECT_ENVIRONMENT), development)
PROJECT_DOMAIN:=api-development.$(PROJECT_DOMAIN)
PROJECT_ENVIRONMENT=development
endif

# in master mode (based git branch)
ifeq ($(PROJECT_ENVIRONMENT), master)
PROJECT_DOMAIN:=api.$(PROJECT_DOMAIN)
PROJECT_ENVIRONMENT=production
endif


docker: docker.up docker.log
vagrant: vagrant.up vagrant.status

.ONESHELL:
docker.up: docker.stop docker.pull docker.build
	docker-compose up --detach --remove-orphans

.ONESHELL:
docker.stop:
	docker-compose stop --timeout 10

.ONESHELL:
docker.log:
	docker-compose logs --follow --tail="all"

.ONESHELL:
docker.pull:
	# need vpn, remove echo
	echo docker-compose pull

.ONESHELL:
docker.build:
	docker-compose build --pull

.ONESHELL:
docker.clean: docker.stop
	docker-compose rm   --force --stop -v
	docker-compose down --remove-orphans

.ONESHELL:
vagrant.up: vagrant.validate
	vagrant up --parallel --destroy-on-error --provider virtualbox

.ONESHELL:
vagrant.halt:
	vagrant halt

.ONESHELL:
vagrant.destroy:
	vagrant destroy

.ONESHELL:
vagrant.reload:
	vagrant reload --provision

.ONESHELL:
vagrant.status:
	vagrant global-status
	vagrant status
	vagrant port db
	vagrant port web

.ONESHELL:
vagrant.validate:
	vagrant validate

.ONESHELL:
vagrant.rsync:
	vagrant rsync-auto

.ONESHELL:
shell.bash:
	exec bash

.ONESHELL:
shell.python:
	$(PYTHON3)

.ONESHELL:
shell.django:
	$(PYTHON3) manage.py shell

.ONESHELL:
shell.vagrant.db:
	vagrant ssh db

.ONESHELL:
shell.vagrant.web:
	vagrant ssh web


install.development: 	os.install.package \
						os.setup.user \
						os.setup.locale \
						os.setup.time \
						os.setup.systemd \
						os.setup.fixperm \
						django.development


install.production:		os.install.package \
						os.setup.user \
						os.setup.locale \
						os.setup.time \
						os.setup.systemd \
						os.setup.fixperm \
						app.nginx.install \
						app.letsencrypt.setup \
						app.nginx.setup \
						app.memcached.setup \
						app.rabbitmq.setup \
						django.production


django.development: venv django.prepare-local-settings django.migrate django.files django.loaddata django.messages  django.test django.check django.test django.lint
django.production: venv django.migrate django.files django.loaddata django.messages django.migrate django.test django.check-deploy

.ONESHELL:
os.setup.repo:
	sed -i 's|http://archive.ubuntu.com/ubuntu|mirror://mirrors.ubuntu.com/mirrors.txt|g' /etc/apt/sources.list
	sed -i '/bionic partner/s/^# //' /etc/apt/sources.list

.ONESHELL:
os.install.update: os.setup.repo
	$(PKG) update
	$(PKG) full-upgrade
	$(PKG) autoclean
	$(PKG) autoremove

.ONESHELL:
os.install.package: os.install.update
	$(PKG) install	build-essential \
					python3-dev python3-pip python3-venv \
					bash-completion command-not-found-data command-not-found \
					htop make nano vim curl wget rsync gettext tree \
					uwsgi-plugin-python3 uwsgi-plugins-all uwsgi-extra uwsgi-dev python3-uwsgidecorators

.ONESHELL:
os.setup.user:
	# if the user does not exist, create it
	id --user $(PROJECT_USER) &>/dev/null || useradd --home-dir $(PROJECT_ROOT_DIRECTORY) --no-create-home $(PROJECT_USER)

	# add user to sudo group
	usermod --append --groups sudo $(PROJECT_USER)

	# allow user run sudo without password
	echo "$(PROJECT_USER) ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/$(PROJECT_USER)

.ONESHELL:
os.setup.locale:
	locale-gen --lang en_US.UTF-8
	locale-gen --lang fa_IR
	localectl set-locale LANG=en_US.UTF-8
	localectl set-locale LANGUAGE=en_US.UTF-8

.ONESHELL:
os.setup.time:
	timedatectl set-timezone $(PROJECT_TIMEZONE)
	timedatectl set-local-rtc false
	timedatectl set-ntp true

.ONESHELL:
os.setup.hostname:
	hostnamectl set-hostname $(PROJECT_DOMAIN)

.ONESHELL:
os.setup.firewall:
	$(PKG) install ufw
	ufw allow 1001

.ONESHELL:
os.setup.ulimit: config.files.generator
	ln -f -s $(PROJECT_DEPLOYMENT_DIRECTORYMENT_DIRECTORY)/output/limits.conf /etc/security/limits.d/$(PROJECT_NAME).conf

.PHONY: os.setup.systemd
.ONESHELL:
## setup systemd services
os.setup.systemd: config.files.generator
	declare -a services_array=("uwsgi.service")

	for i in "$${services_array[@]}"
	do
		echo $$i
		cp $(PROJECT_DEPLOYMENT_DIRECTORY)/output/$$i 	/etc/systemd/system/$(PROJECT_NAME)_$$i
		systemctl daemon-reload
		systemctl --force stop $(PROJECT_NAME)_$$i
		systemctl --force enable $(PROJECT_NAME)_$$i
		systemctl --force restart $(PROJECT_NAME)_$$i
	done

.ONESHELL:
os.setup.fixperm: os.setup.user
	chown --recursive --changes $(PROJECT_USER):$(PROJECT_USER) $(PROJECT_ROOT_DIRECTORY)

.ONESHELL:
app.nginx.install: os.install.package
	$(PKG) install nginx-extras geoip-database geoip-database-extra  nghttp2-client

.ONESHELL:
app.nginx.setup: app.nginx.install
	cp -fv $(PROJECT_DEPLOYMENT_DIRECTORY)/output/nginx.base.*.conf /etc/nginx/conf.d/
	cp -fv $(PROJECT_DEPLOYMENT_DIRECTORY)/output/nginx.site.*.conf /etc/nginx/sites-enabled/
	cp -fv $(PROJECT_DEPLOYMENT_DIRECTORY)/output/*.html /var/www/html/

	cp -fv $(PROJECT_DEPLOYMENT_DIRECTORY)/output/nginx.conf /etc/nginx/nginx.conf

	usermod --append --groups www-data $(PROJECT_USER)
	systemctl --force restart nginx.service

.ONESHELL:
app.memcached.install: os.install.package

.ONESHELL:
app.memcached.setup:

.ONESHELL:
app.postgres.install: os.install.package
	$(PKG) install 	postgresql postgresql-contrib postgresql-client \
					postgresql-common postgresql-all postgresql-server-dev-all


.ONESHELL:
app.postgres.setup:
	# open pg port if UFW is enabled
	ufw allow from any to any port 5432 proto tcp

	PG_VERSION=`pg_config --version | cut -d ' ' -f2 | cut -b 1,2`

	# Edit postgresql.conf to change listen address to '*':
	sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "/etc/postgresql/$$PG_VERSION/main/postgresql.conf"

	# Append to pg_hba.conf to add password auth:
	echo "host	all	all	all	md5" >> "/etc/postgresql/$$PG_VERSION/main/pg_hba.conf"

	# Explicitly set default client_encoding
	echo "client_encoding = utf8" >> "/etc/postgresql/$$PG_VERSION/main/postgresql.conf"


	# Restart so that all new config is loaded:
	service postgresql restart


	# create pg user
	sudo --user=postgres --login --non-interactive psql postgres \
	-c "CREATE ROLE $(DB_USER) LOGIN CONNECTION LIMIT 1000 ENCRYPTED PASSWORD '$(DB_PASS)';" \
	-c "ALTER  ROLE $(DB_USER) SET client_encoding = UTF8;" \
	-c "ALTER  ROLE $(DB_USER) SET timezone = UTC;"

	# create database
	sudo --user=postgres --login --non-interactive psql postgres \
	-c "CREATE DATABASE $(DB_NAME) OWNER = $(DB_USER) TEMPLATE = template0 ENCODING = 'UTF8' CONNECTION LIMIT = 1000;"

	# set PRIVILEGES
	sudo --user=postgres --login --non-interactive psql postgres \
	-c "GRANT  ALL PRIVILEGES ON DATABASE $(DB_NAME) TO $(DB_USER);" \
	-c "ALTER USER $(DB_USER) WITH CREATEDB;"

.ONESHELL:
app.postgres.config:
	@echo DB_HOST: $(DB_HOST)
	@echo DB_PORT: $(DB_PORT)
	@echo DB_NAME: $(DB_NAME)
	@echo DB_USER: $(DB_USER)
	@echo DB_PASS: $(DB_PASS)

.ONESHELL:
app.rabbitmq.install: os.install.package
	$(PKG) install rabbitmq-server

.ONESHELL:
app.rabbitmq.setup:
	rabbitmq-plugins enable rabbitmq_management
	rabbitmqctl add_user $(RABBITMQ_USER) $(RABBITMQ_PASS)
	rabbitmqctl set_user_tags $(RABBITMQ_USER) administrator
	rabbitmqctl set_permissions -p / $(RABBITMQ_USER) ".*" ".*" ".*"

.ONESHELL:
app.letsencrypt.install: os.install.package
	$(PKG) install install software-properties-common
	add-apt-repository ppa:certbot/certbot -y
	$(PKG) update
	$(PKG) install python-certbot-nginx

.ONESHELL:
app.letsencrypt.setup:

venv:$(PROJECT_VENV_DIRECTORY)/bin/activate

.ONESHELL:
$(PROJECT_VENV_DIRECTORY)/bin/activate:
	test -d $(PROJECT_VENV_DIRECTORY) || python3 -m venv $(PROJECT_VENV_DIRECTORY)
	pip3 install --no-cache-dir --upgrade --compile pip
	pip3 install --no-cache-dir --upgrade --compile --requirement requirements/$(PROJECT_ENVIRONMENT).txt
	touch $(PROJECT_VENV_DIRECTORY)/bin/activate
	touch requirements/*.txt


.ONESHELL:
config.files.generator: venv
	for template_file in $(PROJECT_DEPLOYMENT_DIRECTORY)/templates/*; do \
	echo "Processing template file \"$$template_file\""; \
	perl -p -e 's/\$$\{\{\s*(\w+)\s*\}\}/defined $$ENV{$$1} ? $$ENV{$$1} : "<TEMPLATE_VAR_NOT_FOUND>"/eg' < $$template_file > $(PROJECT_DEPLOYMENT_DIRECTORY)/output/$$(basename $$template_file); \
	done
	grep -qr "<TEMPLATE_VAR_NOT_FOUND>" $(PROJECT_DEPLOYMENT_DIRECTORY) && echo "Warning: some template variables were not found" || true


# See <https://gist.github.com/klmr/575726c7e05d8780505a> for explanation.
.ONESHELL:
show-help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)";echo;sed -ne"/^## /{h;s/.*//;:d" -e"H;n;s/^## //;td" -e"s/:.*//;G;s/\\n## /---/;s/\\n/ /g;p;}" ${MAKEFILE_LIST}|LC_ALL='C' sort -f|awk -F --- -v n=$$(tput cols) -v i=19 -v a="$$(tput setaf 6)" -v z="$$(tput sgr0)" '{printf"%s%*s%s ",a,-i,$$1,z;m=split($$2,w," ");l=n-i;for(j=1;j<=m;j++){l-=length(w[j])+1;if(l<= 0){l=n-i-length(w[j])-1;printf"\n%*s ",-i," ";}printf"%s ",w[j];}printf"\n";}'|more $(shell test $(shell uname) == Darwin && echo '-Xr')


.ONESHELL:
django.prepare-local-settings:
	@if [ ! -f $(PROJECT_SOURCE_DIRECTORY)/core/settings/local.py ]; then
		@echo "settings/local.py not found!"
	else
		echo "settings/local.py found!"
	fi

.ONESHELL:
django.files: venv
	$(PYTHON3) $(DJANGO_MANAGE_FILE) collectstatic --noinput

.ONESHELL:
django.loaddata: venv
	$(PYTHON3) $(PROJECT_SOURCE_DIRECTORY)/manage.py loaddata --ignorenonexistent \
								fake.billing.flavorpricing.json \
								fake.billing.project.json \
								fake.billing.userlevel.json \
								fake.wallet.defaultsuggestedprice.json \
								fake.wallet.wallet.json

.ONESHELL:
django.messages: venv
	cd $(PROJECT_SOURCE_DIRECTORY) && $(PYTHON3) $(DJANGO_MANAGE_FILE) makemessages --locale fa --locale en --no-wrap  --verbosity 2
	cd $(PROJECT_SOURCE_DIRECTORY) && $(PYTHON3) $(DJANGO_MANAGE_FILE) compilemessages --locale fa --locale en --verbosity 2

.ONESHELL:
django.migrate: venv
	$(PYTHON3) $(DJANGO_MANAGE_FILE) makemigrations --verbosity 2
	$(PYTHON3) $(DJANGO_MANAGE_FILE) migrate --verbosity 2

.ONESHELL:
django.runserver: venv
	$(PYTHON3) $(DJANGO_MANAGE_FILE) runserver --verbosity 2 0.0.0.0:8080

.ONESHELL:
django.createsuperuser: venv
	$(PYTHON3) $(DJANGO_MANAGE_FILE) createsuperuser

.ONESHELL:
django.uwsgi: venv
	cd $(PROJECT_SOURCE_DIRECTORY) && uwsgi --ini $(PROJECT_DEPLOYMENT_DIRECTORY)/configuration/templates/uwsgi.ini --http 0.0.0.0:8080

.ONESHELL:
django.gunicorn: venv
	cd $(PROJECT_SOURCE_DIRECTORY) && gunicorn --workers=1 --bind=0.0.0.0:8080 $(PROJECT_NAME).wsgi:application

.ONESHELL:
django.test: venv
	$(PYTHON3) $(DJANGO_MANAGE_FILE) test

.ONESHELL:
django.check: venv
	$(PYTHON3) $(DJANGO_MANAGE_FILE) check

.ONESHELL:
django.check-deploy: venv
	$(PYTHON3) $(DJANGO_MANAGE_FILE) check --deploy

.ONESHELL:
django.celery.worker: venv
	cd $(PROJECT_SOURCE_DIRECTORY) && celery worker --app core

.ONESHELL:
django.celery.beat: venv
	cd $(PROJECT_SOURCE_DIRECTORY) && celery beat --app core

.ONESHELL:
django.lint: venv
	flake8 --max-line-length=119 --exclude=*/migrations/* src | nl
